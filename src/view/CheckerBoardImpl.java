package view;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import controller.ControllerImpl;
import model.CheckerBoardShadow.PiecesType;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * implementation of {@link CheckerBoard} interface.
 *
 */
public class CheckerBoardImpl extends JFrame implements CheckerBoard 
{
    private static final long serialVersionUID = -6218820567019985015L;
    private final Map<JButton,Pair<Integer,Integer>> buttons = new HashMap<>();
    private final Map<Pair<Integer,Integer>,DamaBoxImpl> pos = new HashMap<>();

    /**
     * Constructor
     */
    public CheckerBoardImpl(int rows, int cols, ControllerImpl controller)
    {

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(600, 600);
        JPanel panel = new JPanel(new GridLayout(rows,cols));
        String title = "Dama checkerBoard";
        Border border = BorderFactory.createTitledBorder(title);
        panel.setBorder(border);
        this.getContentPane().add(BorderLayout.CENTER,panel);
        //center of the screen
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);

        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            final Pair<Integer,Integer> p = buttons.get(bt);
            controller.click(p);

        };

        for (int r=0; r<rows; r++)
        {
            for (int c=0; c<cols; c++)
            {
                final DamaBoxImpl jb = new DamaBoxImpl();

                jb.setBackground(Color.white);


                if((r%2==0 && c%2==0)||(r%2==1 && c%2==1)) 
                {
                    jb.setBackground(Color.gray);
                }

                jb.addActionListener(al);
                this.buttons.put(jb,new Pair<>(r,c));
                this.pos.put(new Pair<>(r,c),jb);
                panel.add(jb);

            }
        }
        this.setVisible(true);
        setResizable(false);
    }


    public void setPieces(int x, int y, PiecesType pt) throws IOException 
    {
        this.pos.get(new Pair<>(x,y)).setPiece(pt);
    }

    public void resetRedBorder() 
    {
        for (DamaBoxImpl d : this.pos.values())
        {
            d.setBorder(null);
        }
    }

    public void setBorder(int x,int y)
    { //color,thickness
        this.pos.get(new Pair<>(x,y)).setBorder(new LineBorder(Color.RED,5));
    }


    @Override
    public void close() {
        this.setVisible(false);
        this.dispose();
    }

}