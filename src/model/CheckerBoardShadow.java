package model;

import java.util.List;
import view.Pair;

/**
 * CheckerBoardShadow interface
 */
public interface CheckerBoardShadow {

    /**
     * Player color
     *  <li>{@link #playerWhite}</li>
     *  <li>{@link #playerBlack}</li>
     */
    public  enum PlayerName {
        /**
         * turn of  the player that moves white pieces
         */
        playerWhite,
        /**
         * turn of  the player that moves black pieces
         */
        playerBlack
    }

    /**
     * pieces on the checkerBoard
     * <li>{@link #WP}</li> 
     * <li>{@link #BP}</li> 
     * <li>{@link #WD}</li> 
     * <li>{@link #BD}</li> 
     * <li>{@link #E}</li> 
     */
    public enum PiecesType {
        /**
         * whitePiece
         */
        WP,
        /**
         * blackPiece
         */
        BP,
        /**
         * whiteDama
         */
        WD,
        /**
         * blackDama
         */
        BD,
        /**
         * empty
         */
        E;
    }

    /**
     * 
     * @param x {@link integer} of the piece
     * @param y {@link integer} of the piece
     * @return the type of the piece at the coordinates (x,y) could be WP,WD,BP,BD
     */
    PiecesType getType (int x, int y);

    /**
     * 
     * @param x {@link integer} of the piece
     * @param y {@link integer} of the piece
     * @param pt {@link PiecesType} set the piece (WD, WP, BD, BP) at the coordinates (x,y)
     * @return
     */
    PiecesType setType (int x, int y, PiecesType pt);

    /**
     * 
     * @param x {@link integer} coordinate of the piece selected
     * @param y {@link integer} coordinate of the piece selected
     * @param {@link List} availableRedpath List of the red Path based on the piece selected
     */
    void selectItem(int x,int y, List<Pair<Integer,Integer>> availableRedpath );

    /**
     * 
     * @return {@link boolean} true if the player has selected a piece
     */
    boolean isSelected();

    /**
     * reset of the piece selected
     */
    void unselectItem();

    /**
     * 
     * @param p {@link List} list of the red Paths available
     * @return {@link List} list of the red Paths available
     */
    List<Pair<Integer, Integer>> redPathCoordinates(List<Pair<Integer,Integer>> p);

    /**
     * 
     * @return {@link PlayerName} the current player color (playerWhite, playerBlack)
     */
    PlayerName getPlayerColor();

    /**
     * move the piece through the checkerBoard for these moves:
     * 1)eat a piece an became a Dama
     * 2)move one position (without eat), and become a Dama
     * 3)eat a piece and don't become a Dama
     * 4)move one position (without eat),and  don't become a Dama
     * @param x {@link integer} coordinate of the checkerBoard
     * @param y {@link integer} coordinate of the checkerBoard
     */
    void movePiece(int x, int y );

    /**
     * coordinates of the piece that had to be eaten
     * @param x {@link integer} coordinate of the piece selected
     * @param y {@link integer} coordinate of the piece selected
     */
    void getEatPiece(int x, int y);

    /**
     * @return {@link Pair}  return the coordinates of the piece that can eat again, after the first time he eat
     */
    Pair<Integer, Integer> getMustEatPiece();

    /**
     * set the Dama at the position(x,y)
     * @param x {@link integer} coordinate
     * @param y {@link integer} coordinate
     */
    void setDama(int x, int y) ;

    /**
     * this method is used only to guarantee the first piece of every game is a WhitePiece WP
     * @param t {@link PiecesType}  the current piece
     * @return {@link integer} the player turn
     */
    int getPlayerTurn(PiecesType t);

    /**
     * check who is the winner: playerBlack or playerWhite
     */
    void winner();

}
