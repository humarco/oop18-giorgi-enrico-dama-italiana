package controller;

import java.io.IOException;
import java.util.List;
import model.CheckerBoardShadow.PiecesType;
import model.CheckerBoardShadow.PlayerName;
import view.Pair;

/**
 * Controller interface
 */
public interface Controller {
	/**
	 * this method initializes  the controller, the view, update the checkerBoard
	 * so the game
	 */
	void startGame();
	/**
	 * start  a new game  after declaring a winner
	 */
	void startNewGame();
	/**
	 * this method contains the logic of the "first click" on current piece, and
	 * the "second click" for select where the piece had to go between the redPath choices
	 * @param {@link Pair} ev Coordinates x,y of the piece selected
	 */
	void click(Pair<Integer,Integer> ev);

	/**
	 * The actual piece can eat again?
	 * @param x {@link integer} of the piece selected
	 * @param y {@link integer} of the piece selected
	 * @param color {@link PlayerName} of the player (playerWhite, playerBlack)
	 * @return true {@link boolean} if i can i eat again with the piece selected
	 */
	boolean canIEatAgain( int x, int y, PlayerName color);

	/**
	 * @param color {@link PlayerName} of the player (playerWhite, playerBlack)
	 * @return {@link List} a list with all the positions(x,y) of the pieces
	 * that can actually eat, based on the color given (playerWhite/playerBlack) 
	 */
	List<Pair<Integer,Integer>> getPiecesPriorityToEat(PlayerName color);

	/**
	 * @param t {@link PiecesType} actual piece type (WD, BD, BP, WP)
	 * @param ev {@link Pair} coordinates of actual piece
	 * @return {@link List} a list with all red paths coordinates of actual piece
	 */
	List<Pair<Integer,Integer>> availableRedChoices(PiecesType t, Pair<Integer,Integer> ev);

	/**
	 * this method deals with the rules of a Dama piece (they can move on every diagonal)
	 * @param t {@link PiecesType} current Dama
	 * @param ev {@link Pair} coordinates of current Dama
	 * @return {@link List} a list of Red Path coordinates where i can move my Dama
	 */
	List<Pair<Integer,Integer>> damaRoute(PiecesType t, Pair<Integer, Integer> ev);

	/**
	 * this method deal with the logic of change a  normal piece to create a Dama.
	 * @param t {@link PiecesType} current piece 
	 * @param ev {@link Pair} coordinates of current piece 
	 * @return {@link List} a list of red path coordinates where i can set (or not set) the Dama
	 */
	List<Pair<Integer,Integer>> createDama(PiecesType t, Pair<Integer, Integer> ev);

	/**
	 * Update the checkerBoard with pieces taken from checkerBoardShadow
	 * @throws IOException 
	 */
	void updateCheckerBoard(); 
	

}
